**********************
Command Line Interface
**********************

This document is a prettier output of the documentation produced by
the command line client's ``man`` command. You can obtain similar pages
using the following shell commands.

::

   tezos-client -protocol ProtoALphaALph man -verbosity 3
   tezos-admin-client man -verbosity 3


.. _client_manual:
.. _client_manual_007:

Client manual
=============

.. raw:: html
         :file: tezos-client-007.html


.. _admin_client_manual:
.. _admin_client_manual_007:

Admin-client manual
===================

.. raw:: html
         :file: ../api/tezos-admin-client.html


.. _signer_manual:
.. _signer_manual_007:

Signer manual
=============

.. raw:: html
         :file: ../api/tezos-signer.html


.. _baker_manual:
.. _baker_manual_007:

Baker manual
============

.. raw:: html
         :file: tezos-baker-007.html


.. _endorser_manual:
.. _endorser_manual_007:

Endorser manual
===============

.. raw:: html
         :file: tezos-endorser-007.html


.. _accuser_manual:
.. _accuser_manual_007:

Accuser manual
==============

.. raw:: html
         :file: tezos-accuser-007.html
